ansible-role-power
==================

.. _`wakeonlan module`: https://docs.ansible.com/ansible/wakeonlan_module.html

A collection of Ansible roles for powering up (wake on LAN), rebooting and 
powering down hosts. Motivation behind this project is a simple fact. Ansible 
`wakeonlan module`_  does not provide any support for more complex network 
scenarios - e.g. when sending a magic WOL packet to an 255.255.255.255 / 
FF:FF:FF:FF:FF:FF is not enough. An example of such scenario may be a network 
for computer labs. Each lab has its private segment and network communication 
is beeing routed (to internet, to another segment with servers etc.). To use 
`wakeonlan module`_ one would need a control machine in each network (it's not 
a big issue to run a VM for each one but there are other options).

Big picture
-----------

.. _`power/wol-agent`: roles/power/wol-agent/README.rst
.. _`power/wol`: roles/power/wol/README.rst
.. _`power/waitfor`: roles/power/waitfor/README.rst
.. _`power/shutdown`: roles/power/shutdown/README.rst
.. _`power/reboot`: roles/power/reboot/README.rst
.. _`wakeonlan`: http://gsd.di.uminho.pt/jpo/software/wakeonlan/
.. _`Gentoo`: https://packages.gentoo.org/packages/net-misc/wakeonlan
.. _`Debian`: https://packages.debian.org/stable/wakeonlan

This project uses a special host _agent_ that can access all the networks, thus
it can send a magic wake on LAN packet. The project uses `wakeonlan`_ package 
(Debian_, Gentoo_) to do so.::

    control  -send WOL packet-> agent -broadcast-> lab hosts
    machine

To install the _agent_ please refer to documentation of `power/wol-agent`_ role. 

To use WOL functionality please refer to documentation of `power/wol`_ role. To wait
for hosts on a lab network use `power/waitfor`_ role.

    To reboot or shutdown hosts use `power/reboot`_ or `power/shutdown`_ role.

Installation
------------

Copy the roles to the ``roles`` directory of yours. If you are using git for your
playbooks you can add the roles as a git subodule wit a simple command: ::

    git submodule add git@gitlab.com:tomaskadlec/ansible-role-power.git roles/power
    
