power-wol
=========

*power/wol* is an Ansible role that allows to power up hosts using magic wake 
on LAN packet.

Requirements
------------

  * Ansible >=1.9 (tested with Ansible 2.2.x)
  * an _agent_ that

    * was installed using `power/wol-agent <../wol-agent/README.rst>`_ role,
    * has access to a network with hosts to wake up

Usage
-----

The role wakes up hosts accodring to ``hosts`` and ``--limit``. All the work is delegated
to an appropriate _agent_ which must be configured. Each host may have a different
_agent_ - it is configured as just another Ansible variable.

wol_ip_host (required, alternates with wol_ip_broadcast)
    An IPv4 address of host to wake up (only /24 netmask is supported now).

wol_ip_broadcast (required, alternates with wol_ip_host)
    An IPv4 broadcast address.

wol_mac
    MAC address of host to wake up.

An example usage may look as follows. ::

```yaml

-   name: "send magic wake on LAN packet"
    hosts: [hosts]
    gather_facts: no
    serial: 3
    roles:
        - role: power/wol
          agent: "{{host_wol_agent}}"
          wol_ip_broadcast: "{{host_broadcast_address}}"
          wol_mac: "{{host_mac_address}}"
```

Please note several interesting thigs in the example.

  * ``gather_facts`` is set to ``no`` for the play. It is not possible to contact hosts, they are down.
  * ``serial`` is set to 3 (or any small number). It prevents from exhausting resources (electricity, DHCP/TFTP requests).
  * Variables are used to configure the role. You can use them even for __the agent__.

That's quite it. Your hosts are up and running now ;)
