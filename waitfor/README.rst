power/waitfor
=============

*power/waitfor* is an Ansible role that waits for hosts to come up.
It is useful after powering up / rebooting the hosts.

Requirements
------------

  * Ansible >=1.9 (tested with Ansible 2.2.x)

Usage
-----

Configure the role at first.

``power_waitfor.delay`` (defaults to 30 seconds)
  Delay before a host is tryied for the first time.

``power_waitfor.timeout`` (defaults to 180 seconds)
  Host is marked as unreachable after the timeout expires.

Waiting is executed locally - on the controller host. It is important to disable 
automatic gathering of facts. Such usage is ilustrated on the following excerpt: ::

  - name: "Wait for hosts"
    gather_facts: no
    hosts: [hosts]
    roles:
        - { role: "power/waitfor" }

