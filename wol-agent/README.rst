power/wol-agent
===============

``power/wol-agent`` is an Ansible role that installs required software on 
a host(s) that will act as an _agent(s)_. Requests to send wake on LAN 
magic packets will be delegated to it (them).

Requirements
------------

  * Ansible >=1.9 (tested with Ansible 2.2.x)
  * A host that will act as an _agent_ that

    * runs Debian or Gentoo GNU/Linux,
    * has access to all networks where host should be woken up.

Usage
-----

Just include the role in your play regarding the _agent_ (or _agents_).

```yaml

- name: "WOL - agent"
  hosts: [u-wol]
  roles:
    - { role: power/wol-agent }
```
