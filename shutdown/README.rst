power/shutdown
==============

*power/shutdown* is an Ansible role that allows shutting down hosts in the play.

Requirements
------------

  * Ansible >=1.9 (tested with Ansible 2.2.x)

Usage
-----

Shutdown is performed after a delay ``power_shutdown.delay``. It defaults to 2
seconds. Example usage follows:

```
- name: "Shutdown hosts in the 'current' group"
  serial: 3
  gather_facts: no
  any_errors_fatal: ~
  hosts: current
  roles:
    - { role: "power-shutdown" }
```
