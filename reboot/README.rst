power/reboot
============

*power/reboot* is an Ansible role that allows rebooting hosts in the play.

Requirements
------------

  * Ansible >=1.9 (tested with Ansible 2.2.x)

Usage
-----

Reboot is performed after a delay ``power_reboot.delay``. It defaults to 2
seconds. Example usage is shown bellow. ::

    -   name: "Reboot hosts in the 'current' group"
        serial: 3
        gather_facts: no
        any_errors_fatal: ~
        hosts: [hosts[
        roles:
            - { role: "power-reboot" }

Note that hosts are not rebooted at once using ``serial`` property. It prevents
resources (electricity, DHCP or TFTP) to be exhausted after hosts reboot.
